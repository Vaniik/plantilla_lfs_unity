﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {

	// Use this for initialization
	[SerializeField] float bumperForce=0F;
	private GameObject Ball;
	void Start () {
		Ball=GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}

	void OnTriggerEnter(Collider collider)
	{
		if(collider.gameObject==Ball){
			Ball.GetComponent<Rigidbody>().AddForce(transform.forward*bumperForce);
			
		}
	}
}
